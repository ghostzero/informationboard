<?php

class Model_ContentSubcategory extends ORM
{
    protected $_table_name = 'content_subcategory';
    protected $_primary_key = 'id';
//    protected $_has_one=['Subcategory'=>['model' => 'ContentSubcategory','foreign_key' => 'subcategory_id']];
    protected $_belongs_to = array('Subcategory' => array('model' => 'Subcategory', 'foreign_key' => 'subcategory_id',));
    protected $_has_many = ['FootballGames' => ['model' => 'FootballGames', 'foreign_key' => 'content_subcategory_id']];

    public function get_content_subcategory($subcategory_id)
    {
        return ORM::factory('ContentSubcategory')->where('subcategory_id', '=', $subcategory_id)->find_all();
    }


}

?>