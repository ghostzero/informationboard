<?php

class Model_FootballFora extends ORM
{
    protected $_table_name = 'football_fora';
    protected $_primary_key = 'id';
    protected $_belongs_to = array(
        'FootballGames' => array(
            'model' => 'FootballGames',
            'foreign_key' => 'football_games_id',
        )
    );
}

?>