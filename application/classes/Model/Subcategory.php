<?php

class Model_Subcategory extends ORM {
	protected $_table_name = 'subcategory';
	protected $_primary_key = 'id';
	protected $_has_many = [ 'ContentSubcategory' => [ 'model' => 'ContentSubcategory', 'foreign_key' => 'subcategory_id' ] ];
	protected $_belongs_to = [
		'Category' => [
			'model'       => 'Category',
			'foreign_key' => 'id',
		]
	];

	/**
	 * @return array
	 * @throws Kohana_Exception
	 */
	public function get_subcategory( $category_id ) {
		$result = ORM::factory( 'Subcategory' )->where( 'category_id', '=', $category_id )->find_all();
		return $result;
	}

	public function get_object_subcategory_by_id($subcategory_id){
		return ORM::factory('Subcategory')->where('id','=',$subcategory_id)->find();
	}
}

?>