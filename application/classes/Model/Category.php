<?php

class Model_Category extends ORM
{
    protected $_table_name = 'category';
    protected $_primary_key = 'id';
    protected $_has_many = ['Subcategory' => ['model' => 'Subcategory', 'foreign_key' => 'category_id']];
    protected $_has_one = ['Difference' => ['model' => 'Difference', 'foreign_key' => 'category_id']];

    public function get_name_categories()
    {
        return $this->factory('Category')->find_all()->as_array();
    }

    public function get_objects_category_by_id($id_category)
    {
        return ORM::factory('Category')->where('id','=',$id_category)->find();
    }

}

?>