<?php

class Model_FootballTotull extends ORM
{
    protected $_table_name = 'football_totul';
    protected $_primary_key = 'id';
    protected $_belongs_to = array(
        'FootballGames' => array(
            'model' => 'FootballGames',
            'foreign_key' => 'football_games_id',
        )
    );
}

?>