<?php

class Model_StringLabelsDay extends ORM {
	protected $_table_name = 'string_labels_day';
	protected $_primary_key = 'id';
	protected $_belongs_to = array(
		'FootballGames' => array(
			'model'       => 'FootballGames',
			'foreign_key' => 'id_date',
		)
	);
	protected $_has_many = [ 'FootballFora'   => [ 'model' => 'FootballFora', 'foreign_key' => 'football_games_id' ],
	                         'FootballTotull' => [ 'model' => 'FootballTotull', 'foreign_key' => 'football_games_id' ]
	];


	public function get_name_categories_of_content_subcategory_id( $content_subcategory_id ) {
		return ORM::factory( 'FootballGames' )->where( 'content_subcategory_id', '=', $content_subcategory_id )->find_all();

	}

}

?>