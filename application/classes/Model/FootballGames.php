<?php

class Model_FootballGames extends ORM
{
    protected $_table_name = 'football_games';
    protected $_primary_key = 'id';
    protected $_belongs_to = array(
        'ContentSubcategory' => array(
            'model' => 'ContentSubcategory',
            'foreign_key' => 'content_subcategory_id',
        ),
        'StringLabelsDay' => ['model' => 'StringLabelsDay', 'foreign_key' => 'id_date']
    );
    protected $_has_many = [
        'FootballFora' => ['model' => 'FootballFora', 'foreign_key' => 'football_games_id'],
        'FootballTotull' => ['model' => 'FootballTotull', 'foreign_key' => 'football_games_id']
    ];


    public function get_name_categories_of_content_subcategory_id($content_subcategory_id)
    {
        $result = ORM::factory('FootballGames')->where('content_subcategory_id', '=', $content_subcategory_id)->find_all();
        if ($result->count() == 0)
            return false;
        else
            return $result;

    }

}

?>