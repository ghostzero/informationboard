<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Adminka extends Controller_Base
{
    public $template = '/adminka/index.tpl';

    public function before()
    {
        if ($_POST) {
            ORM::factory('Difference')->set_difference($_POST['difference']);
        }
        parent::before();
    }

    public function action_index()
    {

        $this->template->index = 'Админка';

    }

}
