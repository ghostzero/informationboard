<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Base extends Controller_Template
{
    public function before()
    {
        parent::before();
        $main = ORM::factory('Category')->get_name_categories();
//        $main=false;
        $array_bread_crumbs = $this->get_bread_crumbs_for_football_v_1_2();
        $difference = ORM::factory('Category')->where('id', '=', 1)->find()->Difference->difference;
        View::set_global(['tpl_dir' => APPPATH . "views",
            'title' => 'Информационная таблица',
            'main' => $main,
            'difference' => $difference,
            'array_bread_crumbs' => $array_bread_crumbs
        ]);
    }

    public function get_bread_crumbs_for_football()
    {
        $position_now = $this->request->action();
        $type = $this->request->query('type');
        $array_bread_crumbs = [];
        switch ($position_now) {
            case 'category': {
                $result = ORM::factory('Category')->where('id', '=', $type)->find();
                $array_bread_crumbs = [['href' => 'category?type=' . $result->id, 'name' => $result->name]];
                break;
            }
            case 'subcategory': {
                $result = ORM::factory('Subcategory')->where('id', '=', $type)->find();
                $result_category = $result->Category->find();
                $array_bread_crumbs = [['href' => 'category?type=' . $result_category->id, 'name' => $result_category->name], ['href' => $position_now . '?type=' . $result->id, 'name' => $result->name]];
                $test = 'asdf';
                break;
            }
            case 'content_subcategory': {
                $result = ORM::factory('ContentSubcategory')->where('id', '=', $type)->find();
                try {
                    $name_category = $result->Subcategory->Category->find()->name;
                    $id_category = $result->Subcategory->Category->find()->id;
                } catch (Exception $e) {
                    $name_category = $result->Subcategory->Category->name;
                    $id_category = $result->Subcategory->Category->id;
                }
                $array_bread_crumbs = [['href' => 'category?type=' . $id_category, 'name' => $name_category], ['href' => 'subcategory?type=' . $result->Subcategory->id, 'name' => $result->Subcategory->name], ['href' => $position_now . '?type=' . $result->id, 'name' => $result->name]];
                break;
            }
        }

        return $array_bread_crumbs;
    }

    public function get_bread_crumbs_for_football_v_1_2()
    {
        $position_now = $this->request->action();
        $type = $this->request->query('type');
        $array_bread_crumbs = [];
        switch ($position_now) {
            case 'category': {
                $result = ORM::factory('Category')->where('id', '=', $type)->find();
                $array_bread_crumbs = [['href' => 'category?type=' . $result->id, 'name' => $result->name]];
                break;
            }
            case 'subcategory': {
                $result = ORM::factory('Subcategory')->where('id', '=', $type)->find();
                $category = ORM::factory('Category')->get_objects_category_by_id($result->category_id);
                $array_bread_crumbs = [['href' => 'category?type=' . $category->id, 'name' => $category->name], ['href' => $position_now . '?type=' . $result->id, 'name' => $result->name]];
                $test = 'asdf';
                break;
            }
            case 'content_subcategory': {
                $result = ORM::factory('ContentSubcategory')->where('id', '=', $type)->find();
                $base_subcategory=ORM::factory('Subcategory')->get_object_subcategory_by_id($result->subcategory_id);
                $base_category=ORM::factory('Category')->get_objects_category_by_id($base_subcategory->category_id);
                $array_bread_crumbs = [['href' => 'category?type=' . $base_category->id, 'name' => $base_category->name], ['href' => 'subcategory?type=' . $base_subcategory->id, 'name' => $base_subcategory->name], ['href' => $position_now . '?type=' . $result->id, 'name' => $result->name]];
                break;
            }
        }

        return $array_bread_crumbs;
    }
} // End Welcome
