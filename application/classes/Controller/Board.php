<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Board extends Controller_Base
{
    public $template = 'board/index.tpl';

    /*public function __construct(){

    }*/
    public function before()
    {
        parent::before();
    }

    public function action_index()
    {
        $this->template->index = 'Для просмотра категории в верхней меню выбирите тип категории';
    }

    public function action_category()
    {
        $category_id = $this->request->query('type');
        $content_category_football = View::factory('type_category/football.tpl');
        $array_football = ORM::factory('Subcategory')->get_subcategory($category_id);
        $content_category_football->array_football = $array_football;
        $this->template->content_category_football = $content_category_football;
    }

    public function action_subcategory()
    {
        $template_subcatory_of_type = View::factory('type_subcategory/football.tpl');
//        $template_subcatory_of_type->array_content_subcategory = ORM::factory('');
        $this->template->template_subcatory_of_type = $template_subcatory_of_type;
    }

    public function action_content_subcategory()
    {
        $content_subcategory_id = $this->request->query('type');
        $template_content_subcatory_of_type = View::factory('type_content_subcategory/football.tpl');
        $array_football_games=ORM::factory('FootballGames')->get_name_categories_of_content_subcategory_id($content_subcategory_id);
        $template_content_subcatory_of_type->array_football_games =$array_football_games;

        //Выбрать все по данной под под категории (третий уровень вложенности).
//        $array;
        $this->template->template_content_subcatory_of_type = $template_content_subcatory_of_type;
    }


} // End Welcome
