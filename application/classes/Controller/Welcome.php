<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Welcome extends Controller_Template {

	public $template = 'welcome.tpl';

	public function action_index()
	{
		// Assign a value to the variable 'intro'
		$this->template->intro = 'Hello world!';

		// Create a nested view by loading a different template
		$this->template->content = View::factory('content.tpl');
	}

} // End Welcome
