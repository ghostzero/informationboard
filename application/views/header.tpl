<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>{$title}</title>
    <link rel="stylesheet" media="screen" href="/css/reset.css" type="text/css"/>
    <link rel="stylesheet" media="screen" href="/css/common.css" type="text/css"/>
    <link rel="stylesheet" media="screen" href="/css/ui-lightness/jquery-ui-1.8.5.custom.css" type="text/css"/>
    <script src="/js/jquery-1.4.3.min.js" type="text/javascript"></script>
    <script src="/js/jquery-ui-1.8.5.custom.min.js" type="text/javascript"></script>
    <script src="/js/common.js" type="text/javascript"></script>
</head>

<body>
<div id="container">
    <div id="header">
        <div id="logo">
            Информационная таблица спортивных игр
        </div>
    </div>
    {if isset($main)}
        <div id="menu">
            <ul id="nav1">
                {foreach from=$main item=one_main}
                <li><a href="/category?type={$one_main->id}">{$one_main->name}</a>
                    {/foreach}
            </ul>
            <span class="right"> <a href="/adminka/index">Админка</a> </span>
        </div>
    {/if}
    {foreach from=$array_bread_crumbs item=one_bread_crumb name=bread_crumb}
        <span class="bread_crumb">
        <a href="{$one_bread_crumb.href}">{$one_bread_crumb.name}</a>
        </span>{if !$smarty.foreach.bread_crumb.last}->{/if}
    {/foreach}

    {*{$main}*}
    {*<div id="wrapper">*}
