Содержане футбола
<table class="table_subcategory">
    <tr>
        <th class="number_colum">№</th>
        <th class="name_subcategory">Название подкатегории</th>
    </tr>
    {foreach from=$array_football item=one_subcategory}
        <tr>
            <td>{$one_subcategory->id}</td>
            <td>{$one_subcategory->name}
                <ul>
                    {foreach from=$one_subcategory->ContentSubcategory->where('status','=',1)->find_all() item=contentsubcategory}
                        <li>
                            <a href="/content_subcategory?type={$contentsubcategory->id}">{$contentsubcategory->name}</a>
                        </li>
                    {/foreach}
                </ul>
            </td>
            {*<td>{$one_subcategory->category_id}</td>*}
        </tr>
    {/foreach}
</table>

