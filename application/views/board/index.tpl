{include file="$tpl_dir/header.tpl"}
<div class="content">
    {if isset($index)}
        {$index}
    {/if}
    {if isset($content_category_football)}
        {$content_category_football}
    {/if}
    {if isset($template_subcatory_of_type)}
        {$template_subcatory_of_type}
    {/if}
    {if isset($template_content_subcatory_of_type)}
        {$template_content_subcatory_of_type}
    {/if}
</div>
{include file="$tpl_dir/footer.tpl"}