Один из подкатегорий <br/>
{if isset($array_football_games)&&$array_football_games}
    <span>Список игр</span>
    {*<ul>
        {foreach from=$array_football_games item=one_football_game}
            <li>{$one_football_game->name_first_command} {$one_football_game->name_second_command}
                <ul>
                    {foreach from=$one_football_game->FootballFora->find_all() item=one_football_fora}
                        <li>|{$one_football_fora->factor_of_fora}|{$one_football_fora->factor_of_money}|</li>
                    {/foreach}
                </ul>
            </li>
        {/foreach}


    </ul>*}
    <table class="table_content_subcategory">
        <tr>
            <th class='th_1'>№</th>
            <th class='th_2'>Дата</th>
            <th class='th_3'>Время</th>
            <th class='th_4'>Название события</th>
            <th class='th_5'>1</th>
            <th class='th_6'>Х</th>
            <th class='th_7'>2</th>
            <th class='th_8'>Фора</th>
            <th class='th_9'>Тотул</th>
            <th class='th_10'>Роспись</th>
        </tr>
            {foreach from=$array_football_games item=one_football_game}
                <tr>
                    <td class="td_1 ">{$one_football_game}</td>
                    <td class="td_2 ">{$one_football_game->StringLabelsDay->number_day_and_month}</td>
                    <td class="td_3 ">{$one_football_game->time}</td>
                    <td class="td_4 ">1.{$one_football_game->name_first_command}<br/>
                        2.{$one_football_game->name_second_command}</td>
                    <td class="td_5 ">{$one_football_game->win_first_command-$difference}</td>
                    <td class="td_6 ">{$one_football_game->dead_heat-$difference}</td>
                    <td class="td_7 ">{$one_football_game->win_second_command-$difference}</td>
                    <td class="td_8 ">
                        {foreach from=$one_football_game->FootballFora->find_all() item=one_football_fora}
                            {$one_football_fora->factor_of_fora} | {$one_football_fora->factor_of_money-$difference}
                            <br/>
                        {/foreach}
                    </td>
                    <td class="td_9 ">
                        {foreach from=$one_football_game->FootballTotull->find_all() item=one_football_totul}
                            {$one_football_totul->more_or_less} {$one_football_totul->factor_of_totul} |  {$one_football_totul->factor_of_money-$difference}
                            <br/>
                        {/foreach}
                    </td>
                    <td class="td_10 ">{$one_football_game}</td>
                </tr>
            {/foreach}
    </table>
{/if}